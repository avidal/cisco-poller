from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView

from accounts.forms import AuthenticationForm


# Override a few default strings in the admin site
# See: https://docs.djangoproject.com/en/1.8/ref/contrib/admin/#adminsite-attributes
admin.site.site_header = "The Cisco Poller"
admin.site.site_title = "The Cisco Poller Admin"
admin.site.index_title = "Administration"

# Let's make the admin login page use our pretty login/logout pages
admin.site.login_form = AuthenticationForm
admin.site.login_template = 'accounts/login.html'
admin.site.logout_template = 'accounts/logged_out.html'


urlpatterns = patterns(
    '',
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),

    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^polls/', include('polls.urls', namespace='polls')),

    url(r'^api/', include('api.urls', namespace='api')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
