"""
Django settings for mysite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# BASE_DIR is now the parent of the parent of the location of the settings file:
# __file__ == "/Users/username/Projects/repo-name/mysite/mysite/settings.py
# dirname(__file__) == "/Users/username/Projects/repo-name/mysite/mysite"
# dirname(dirname(__file__)) == "/Users/username/Projects/repo-name/mysite"

# A simple function that allows us to create an absolute path from the BASE_DIR
# Examples:
#  base('templates') => /Users/username/Projects/repo-name/mysite/templates
#  base('etc', 'nginx') => /Users/username/Projects/repo-name/mysite/etc/nginx
base = lambda *p: os.path.join(BASE_DIR, *p)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'sgfifm9&2e)z#dbz$1ow=sg&=t71%*log(2903b+1kves99**!'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third-party Django packages
    'debug_toolbar.apps.DebugToolbarConfig',
    'django_extensions',
    'crispy_forms',
    'rest_framework',

    # Our applications
    'common',
    'accounts',
    'polls',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'mysite.urls'

WSGI_APPLICATION = 'mysite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Chicago'

USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

# STATIC_URL is used when generating a url to a static file. If you're using a CDN, for instance,
# STATIC_URL would be the CDN base url, eg: //cdsq234qf.aws.cloudfront.net/
STATIC_URL = '/static/'

# This is where static files will be collected to when you run the `collectstatic` management
# command, which you need to do when running in production and should be in your ignore file
STATICFILES_ROOT = base('collected_statc')

# Where Django will look for a static file given a particular name; these locations are checked
# first
STATICFILES_DIRS = [base('static')]

# Similar to STATICFILES_DIRS, where Django will look first for a template with a given name
TEMPLATE_DIRS = [base('templates')]

# Adding this setting changes the default tags Django will set on messages created using the
# messages framework.
from django.contrib import messages
MESSAGE_TAGS = {
    messages.SUCCESS: 'alert-success success',
    messages.WARNING: 'alert-warning warning',
    messages.ERROR: 'alert-error error',
}

CRISPY_TEMPLATE_PACK = 'bootstrap3'

# Where to go after logging in. Accepts url pattern names.
LOGIN_REDIRECT_URL = 'index'

# What url to use for login. Accepts url pattern names.
LOGIN_URL = 'accounts:login'

# What url to use for logout. Accepts url pattern names.
LOGOUT_URL = 'accounts:logout'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

REST_FRAMEWORK = {
    'PAGINATE_BY': 10,
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
}
