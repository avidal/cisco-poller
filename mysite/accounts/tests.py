from django.test import TestCase

from accounts.forms import RegistrationForm


class RegistrationFormCleanTests(TestCase):

    def setUp(self):
        data = {'username': 'bad-user-with-a-#',
                'password1': 'password',
                'password2': 'not-password'}
        form = RegistrationForm(data=data)

        form.is_valid()
        self.form = form

    def test_passwords_must_match(self):
        self.assertIn("Passwords must match!",
                      self.form.errors['__all__'])

    def test_username_is_alpha(self):
        self.assertIn('username', self.form.errors)
