from django.core.urlresolvers import reverse
from django.conf import settings
from django.shortcuts import redirect
from django.views import generic

from accounts.forms import RegistrationForm


class RegisterView(generic.FormView):
    form_class = RegistrationForm
    template_name = 'accounts/register.html'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password1'])
        user.save()

        # Send them to the login page after creating a new account.
        return redirect(settings.LOGIN_URL)
