from django.conf.urls import patterns, url

from accounts.forms import AuthenticationForm
from accounts.views import RegisterView


# Override some default keyword arguments for the default login and logout views
# If a view accepts keyword arguments, you can pass them in as the third argument when constructing
# a url.
# See: django/contrib/auth/views.py
login_overrides = {
    'authentication_form': AuthenticationForm,
    'template_name': 'accounts/login.html',
}

logout_overrides = {
    'template_name': 'accounts/logged_out.html',
}


urlpatterns = patterns(
    '',
    url(r'^login/$', 'django.contrib.auth.views.login', login_overrides, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', logout_overrides, name='logout'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
)
