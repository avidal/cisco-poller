import datetime

from django.db import models
from django.utils import timezone


class Question(models.Model):
    user = models.ForeignKey('auth.User')
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    vote_count = models.IntegerField(default=0)

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

    def __unicode__(self):
        return self.question_text


class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    vote_count = models.IntegerField(default=0)

    def __unicode__(self):
        return self.choice_text


class Vote(models.Model):
    user = models.ForeignKey('auth.User')
    question = models.ForeignKey(Question)
    choice = models.ForeignKey(Choice, related_name='votes')

    voted_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [('user', 'question')]
