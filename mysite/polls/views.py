from django.db.models import Count
from django.db import IntegrityError
from django.shortcuts import redirect, render, get_object_or_404
from django.utils import timezone
from django.views import generic

from polls.models import Question, Choice, Vote


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        qs = Question.objects.filter(
            pub_date__lte=timezone.now()
        )

        qs = qs.select_related('user')
        qs = qs.annotate(nchoices=Count('choice'))

        return qs.order_by('-vote_count', '-nchoices')[:100]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'
    context_object_name = 'question'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())

    def get_context_data(self, *args, **kwargs):
        context = super(DetailView, self).get_context_data(*args, **kwargs)
        if self.request.user.is_authenticated():
            has_voted = Vote.objects.filter(user=self.request.user,
                                            question=self.object).exists()
        else:
            has_voted = False
        context['user_has_voted'] = has_voted
        return context


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    p = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.vote_count += 1
        selected_choice.save()

        p.vote_count += 1
        p.save()

        try:
            Vote.objects.create(user=request.user, question=p, choice=selected_choice)
        except IntegrityError:
            # Redisplay the question voting form.
            return render(request, 'polls/detail.html', {
                'question': p,
                'error_message': "You've already voted for this question.",
            })

        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return redirect('polls:results', p.id)
