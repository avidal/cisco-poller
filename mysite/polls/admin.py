from django.contrib import admin
from django.contrib.auth.models import User

from polls.models import Question, Choice


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'pub_date', 'was_published_recently', 'user')
    list_filter = ['pub_date', 'user']
    search_fields = ['question_text', 'user__username']

    fieldsets = [
        (None, {'fields': ['question_text', 'user']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]

    def get_queryset(self, request):
        qs = super(QuestionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "user" and not request.user.is_superuser:
            kwargs['queryset'] = User.objects.filter(id=request.user.id)
        return super(QuestionAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Question, QuestionAdmin)
