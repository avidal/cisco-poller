The Cisco Poller Project
========================

This is our regular tutorial project, but souped up a bit.

You should create a brand new virtualenv for this project, but you can reuse the same database.

If you want to reuse your existing database, copy db.sqlite3 from your old
project to the same level of this one (next to manage.py).

Apps
----

Our project now has three apps:

- common: Currently just holds our template tag library. There's an extra tag in there for great justice.
- accounts: Our login, logout, and registration urls, templates, views, and forms, nicely packaged up.
- polls: The polls app you know and love (hah).

By creating an app just for accounts I was able to remove the
login/logout/register urls from our project (mysite) into accounts. It also let
me move the registration form out of polls.

The common app is a great place to store template tags or utility functions
that you're going to use throughout the site, but it *should not* contain
models.

Things You Should Investigate
-----------------------------

I made a few changes to this project that should demonstrate some more advanced behavior, but I'll leave them as easter eggs. Below is a list with some pointers on where to start:

- The login and logout templates used to be required to exist under
  `registration/`, but I was able to move them under `accounts/`. Can you
  figure out how?
- See how pretty the login form is?
- Admin login and logout are using our login and logout templates.

Homework
--------

Here's a few things you can do on your own:

- Can you make the registration form look nice like the login form?
